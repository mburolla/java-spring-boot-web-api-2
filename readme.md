# Java Spring Boot Web API 2

# Endpoints
- GET http://localhost:8080/api/v1/persons/
- GET http://localhost:8080/api/v1/persons/5150
- GET http://localhost:8080/api/v1/persons?name=fred

# Links
https://youtu.be/vtPkZShrvXQ

# My Notes
- https://start.spring.io/
  - Spring Web
  - H2 Database
  - JDBC API

# MySQL Server Installation Notes
https://dev.mysql.com/downloads/installer/
- MySQL Workbench FULL Install
- Skip the SQL router section
- un: root
- pd: lU6mR3gO9qL8wY1b
