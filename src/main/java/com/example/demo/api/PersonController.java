package com.example.demo.api;

import com.example.demo.model.Person;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonController {

    @GetMapping("api/v1/persons/")
    public Person getPerson() {
        return new Person(1, "Joe");
    }

    @GetMapping("api/v1/persons/{id}")
    public Person getPerson(@PathVariable("id") Integer id) {
        return new Person(id, "Joe");
    }

    @GetMapping("api/v1/persons") // api/v1/persons?name=fred
    public Person getPerson(@RequestParam(required = true) String name) {
        return new Person(1, name);
    }
}
