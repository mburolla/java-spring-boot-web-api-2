package com.example.demo.model;

public class Person {

    //
    // Data Members
    //

    private final Integer id;
    private final String name;

    //
    // Constructors
    //

    public Person(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    //
    // Accessors
    //

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    //
    // Overrides
    //

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
